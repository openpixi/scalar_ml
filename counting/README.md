
## Setup

Download and extract the dataset from [this repository](https://zenodo.org/record/4644550#.YGO3-y2uauU). The global variable `DATASET_PATH` in the Jupyter notebooks has to be changed accordingly.

The conda environment we used for this task is provided in the `env.yml` file. It is created and activated via shell with the following code.

```
conda env create -f env.yml
conda activate scalar_ml_counting_v1
```
## Launching optuna searches

The framework employed for the architecture optimization is `optuna`. In the notebook `Optuna search.ipynb` the hyperparameter space is defined for the three architectures discussed in the paper: EQ, ST and FLAT. Our procedure consists of two independent runs of such a notebook for each type.

Note: depending on your hardware the architecture search can take a few days.

## Optuna result analysis

Since the hyperparameter space is different for each architecture type, we provide three notebooks for the result analysis: `Optuna results - equivariant.ipynb`, `Optuna results - strided.ipynb` and `Optuna results - flattened.ipynb`. There you can visualize the results and check for trends in the hyperparameter choices. For each type, in the next phase we consider only models that made it to the top three considering both runs according to their average validation loss.

## Retraining and testing

The podium of the `optuna` runs is retrained and tested in the notebook `Retraining and testing.ipynb`. The process is not automatic: the architectures have to be manually reported from the previous results. In the training section 20 instances of the same architecture are trained from scratch. Plots showing validation loss and accuracy as functions of different physical parameters are generated in the testing part, where we also provide scatter plots of the best architecture.

## Code overview

* `Optuna search.ipynb` launches the `optuna` search
* `Optuna results - equivariant.ipynb`, `Optuna results - strided.ipynb` and `Optuna results - flattened.ipynb` show the results of the search
* `Retraining and testing.ipynb` retrains the best results and shows the performance on the test set