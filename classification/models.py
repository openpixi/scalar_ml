import torch
import pytorch_lightning as pl
import argparse
from cconv import CConv2d, CMaxPool2d, CAvgPool2d

def nonlinearity():
    return torch.nn.LeakyReLU()

"""
    Equivariant model
"""
class WormClassifier(pl.LightningModule):
    """
        A pytorch-lightning model for worm classification. This model uses a translationally equivariant
        convolutional architecture with an optional dense network at the end of the graph.
    """
    def __init__(self, hparams, train_data, val_data, test_data):
        super(WormClassifier, self).__init__()
        self.hparams = hparams
        
        # set dataset paths 
        self.train_data = train_data
        self.val_data = val_data
        self.test_data = test_data
        
        # copy hyper parameters
        self.biases = hparams.biases.copy()
        self.kernels = hparams.kernels.copy()
        self.channels = hparams.channels.copy()
        if hasattr(hparams, 'eq_pools'):
            self.eq_pools = hparams.eq_pools.copy()
        self.dense_sizes = hparams.dense_sizes.copy()
        self.dense_biases = hparams.dense_biases.copy()
        self.pooling_mode = hparams.pooling_mode
        
        """
            Convolutional network
        """
        # convolutional network is implemented as a pytorch Sequential module
        module_list = []
        
        # add number of inputs (4, due to kt, kx, lt, lx) to the channel list
        self.channels.insert(0, 4)
        for i, ks in enumerate(self.kernels):
            # set in and out channels of CConv2d
            in_ch = self.channels[i]
            out_ch = self.channels[i+1]
            
            # set bias of CConv2d
            bias = self.biases[i]
            
            # create and append to list of modules
            module_list.append(CConv2d(in_ch, out_ch, ks, bias))
            
            # append activation function
            module_list.append(nonlinearity())
            
            # equivariant pooling layer (stride=1)
            if self.eq_pools[i] is not None:
                pool_mode = self.eq_pools[i]
                
                if pool_mode == 'maxpool':
                    pool = CMaxPool2d(2, stride=1)
                elif pool_mode == 'avgpool':
                    pool = CAvgPool2d(2, stride=1)
                else:
                    print("Unknown strided pooling mode '{}'.".format(pool_mode))
                    
                module_list.append(pool) 
        
        # python list is cast to Sequential module
        self.conv = torch.nn.Sequential(*module_list)
        
        """
            Pooling layers
        """
        # check for invalid pooling modes
        valid_pool_modes = ['avgpool', 'sumpool', 'maxpool', 'normavgpool', 'normsumpool']
        if self.pooling_mode not in valid_pool_modes:
            print("Unknown pooling mode '{}'.".format(mode))
            print("Choose from: {}".format(valid_pool_modes))
        
        """
            Dense network
        """
        # dense network is implemented as a pytorch Sequential module 
        module_list = []
        
        # add number of inputs from convolutional network and account for pooling
        self.dense_sizes.insert(0, self.channels[-1])
        self.dense_biases.insert(0, False)
        
        # add number of outputs (1, for prediction)
        self.dense_sizes.append(1)
        self.dense_biases.append(False)
        
        for i in range(len(self.dense_sizes) - 1):
            # set in and out sizes of linear layer
            in_f = self.dense_sizes[i]
            out_f = self.dense_sizes[i+1]
            
            # create and append to list of modules
            module_list.append(torch.nn.Linear(in_f, out_f, bias=self.dense_biases[i]))
            
            # if there is more than one linear layer, add non-linear activations
            if i < len(self.dense_sizes) - 2:
                module_list.append(nonlinearity())
        
        # python list is cast to Sequential module
        self.dense = torch.nn.Sequential(*module_list)
        
        # metrics (loss and accuracy)
        self.vloss = 0.0
        self.vacc = 0.0
        

    def forward(self, x):
        # convolutional network
        x = self.conv(x)
        
        if 'avgpool' == self.pooling_mode:
            # global average pool
            x_avg = x.view(x.shape[0], x.shape[1], -1)
            x_avg = torch.mean(x_avg, dim=2)
            x = x_avg
        elif 'sumpool' == self.pooling_mode:
            # global sum pool
            x_sum = x.view(x.shape[0], x.shape[1], -1)
            x_sum = torch.sum(x_sum, dim=2)
            x = x_sum
        elif 'maxpool' == self.pooling_mode:
            # global max pool
            x_max = x.view(x.shape[0], x.shape[1], -1)
            x_max, _ = torch.max(x_max, dim=2)
            x = x_max
        elif 'normavgpool' == self.pooling_mode:
            # normalized global average pool
            
            # 0) flatten lattice
            x_norm = x.view(x.shape[0], x.shape[1], -1)
            
            # 1) normalize to [0, 1]
            x_min, _ = torch.min(x_norm, dim=2)
            x_norm = x_norm - x_min.view(*x_min.shape, 1)
            x_max, _ = torch.max(x_norm, dim=2)
            x_norm = x_norm / (x_max.view(*x_max.shape, 1) + 1e-8)
            
            # 2) take global average
            x_norm = torch.mean(x_norm, dim=2)
            x = x_norm
        elif 'normsumpool' == self.pooling_mode:
            # normalized global sum pool
            
            # 0) flatten lattice
            x_norm = x.view(x.shape[0], x.shape[1], -1)
            
            # 1) normalize to [0, 1]
            x_min, _ = torch.min(x_norm, dim=2)
            x_norm = x_norm - x_min.view(*x_min.shape, 1)
            x_max, _ = torch.max(x_norm, dim=2)
            x_norm = x_norm / (x_max.view(*x_max.shape, 1) + 1e-8)
            
            # 2) take global sim
            x_sum = torch.sum(x_norm, dim=2)
            x = x_sum
            x = x_min
        
        # dense network
        x = self.dense(x)
        
        # prediction
        x = torch.sigmoid(x)
        
        return x
    
    def loss(self, x, y_true):
        y_pred = self(x).flatten()
        y_true = torch.clamp(y_true, 0, 1)
        loss = torch.nn.functional.binary_cross_entropy(y_pred, y_true)
        return loss
    
    def correct_predictions(self, x, y_true):
        y_pred = self(x).flatten().round()
        y_true = torch.clamp(y_true, 0, 1).round()
        correct = (y_pred == y_true).float().sum()
        return correct

    """
        pytorch_lightning methods
    """

    def prepare_data(self):
        pass

    def train_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.train_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=True, num_workers=self.hparams.num_workers)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.val_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.test_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def configure_optimizers(self):
        self._optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr,
                                            weight_decay=self.hparams.weight_decay, amsgrad=True)
        
        self._scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            self._optimizer,
            T_0=self.hparams.period_epochs,
            T_mult=1,
            eta_min=self.hparams.lr_min,
            last_epoch=-1
        )
        
        return {'optimizer': self._optimizer, 'lr_scheduler': self._scheduler}

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)

        logs = {'loss': loss.cpu().item()}
        return {'loss': loss, 'log': logs}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu().item()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu().item()
        val_acc = val_correct / len(self.val_data)
        
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def test_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu()
        val_acc = val_correct / len(self.test_data)
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def get_progress_bar_dict(self):
        # call .item() only once but store elements without graphs
        running_train_loss = self.trainer.running_loss.mean()
        avg_training_loss = running_train_loss.cpu().item() if running_train_loss is not None else float('NaN')
        lr = self._scheduler.get_last_lr()[0] if self._scheduler is not None else self.hparams.lr
        
        tqdm_dict = {
            'loss': '{:.2E}'.format(avg_training_loss),
            'val_loss': '{:.2E}'.format(self.vloss),
            'val_acc': '{:.2f}'.format(self.vacc),
            'lr': '{:.2E}'.format(lr),
        }

        if self.trainer.truncated_bptt_steps is not None:
            tqdm_dict['split_idx'] = self.trainer.split_idx

        if self.trainer.logger is not None and self.trainer.logger.version is not None:
            tqdm_dict['v_num'] = self.trainer.logger.version

        return tqdm_dict
    
    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
    
"""
    Non-equivariant model (global pooling)
"""

class WormClassifierNEQGP(pl.LightningModule):
    """
        A pytorch-lightning model for worm classification. This model uses convolutions with strided pooling
        layers and a global pooling step.
    """
    def __init__(self, hparams, train_data, val_data, test_data):
        super(WormClassifierNEQGP, self).__init__()
        self.hparams = hparams
        
        # set dataset paths 
        self.train_data = train_data
        self.val_data = val_data
        self.test_data = test_data
        
        # copy hyper parameters
        self.biases = hparams.biases.copy()
        self.kernels = hparams.kernels.copy()
        self.channels = hparams.channels.copy()
        self.strided_pools = hparams.strided_pools.copy()
        self.dense_sizes = hparams.dense_sizes.copy()
        self.dense_biases = hparams.dense_biases.copy()
        self.global_pooling_mode = hparams.global_pooling_mode
        
        """
            Convolutional network
        """
        # convolutional network is implemented as a pytorch Sequential module
        module_list = []
        
        # add number of inputs (4, due to kt, kx, lt, lx) to the channel list
        self.channels.insert(0, 4)
        for i, ks in enumerate(self.kernels):
            # set in and out channels of CConv2d
            in_ch = self.channels[i]
            out_ch = self.channels[i+1]
            
            # set bias of CConv2D
            bias = self.biases[i]
            
            # create and append to list of modules
            module_list.append(CConv2d(in_ch, out_ch, ks, bias))
            
            # append activation function
            module_list.append(nonlinearity())
            
            # strided pooling layer
            if self.strided_pools[i] is not None:
                pool_mode = self.strided_pools[i]
                
                if pool_mode == 'maxpool':
                    pool = torch.nn.MaxPool2d(2)
                elif pool_mode == 'avgpool':
                    pool = torch.nn.AvgPool2d(2)
                else:
                    print("Unknown strided pooling mode '{}'.".format(pool_mode))
                    
                module_list.append(pool)
        
        # python list is cast to Sequential module
        self.conv = torch.nn.Sequential(*module_list)
        
        """
            Pooling layers
        """
        # check for invalid pooling modes
        valid_pool_modes = ['avgpool', 'sumpool', 'maxpool', 'normavgpool', 'normsumpool']
        if self.global_pooling_mode not in valid_pool_modes:
            print("Unknown pooling mode '{}'.".format(self.global_pooling_mode))
            print("Choose from: {}".format(valid_pool_modes))
        
        """
            Dense network
        """
        # dense network is implemented as a pytorch Sequential module 
        module_list = []
        
        # add number of inputs from convolutional network and account for pooling
        self.dense_sizes.insert(0, self.channels[-1])
        self.dense_biases.insert(0, False)
        
        # add number of outputs (1, for prediction)
        self.dense_sizes.append(1)
        self.dense_biases.append(False)
        
        for i in range(len(self.dense_sizes) - 1):
            # set in and out sizes of linear layer
            in_f = self.dense_sizes[i]
            out_f = self.dense_sizes[i+1]
            
            # create and append to list of modules
            module_list.append(torch.nn.Linear(in_f, out_f, bias=self.dense_biases[i]))
            
            # if there is more than one linear layer, add non-linear activations
            if i < len(self.dense_sizes) - 2:
                module_list.append(nonlinearity())
        
        # python list is cast to Sequential module
        self.dense = torch.nn.Sequential(*module_list)
        
        # metrics (loss and accuracy)
        self.vloss = 0.0
        self.vacc = 0.0
        

    def forward(self, x):
        # convolutional network
        x = self.conv(x)
        
        if 'avgpool' == self.global_pooling_mode:
            # global average pool
            x_avg = x.view(x.shape[0], x.shape[1], -1)
            x_avg = torch.mean(x_avg, dim=2)
            x = x_avg
        elif 'sumpool' == self.global_pooling_mode:
            # global sum pool
            x_sum = x.view(x.shape[0], x.shape[1], -1)
            x_sum = torch.sum(x_sum, dim=2)
            x = x_sum
        elif 'maxpool' == self.global_pooling_mode:
            # global max pool
            x_max = x.view(x.shape[0], x.shape[1], -1)
            x_max, _ = torch.max(x_max, dim=2)
            x = x_max
        elif 'normavgpool' == self.global_pooling_mode:
            # normalized global average pool
            
            # 0) flatten lattice
            x_norm = x.view(x.shape[0], x.shape[1], -1)
            
            # 1) normalize to [0, 1]
            x_min, _ = torch.min(x_norm, dim=2)
            x_norm = x_norm - x_min.view(*x_min.shape, 1)
            x_max, _ = torch.max(x_norm, dim=2)
            x_norm = x_norm / (x_max.view(*x_max.shape, 1) + 1e-8)
            
            # 2) take global average
            x_norm = torch.mean(x_norm, dim=2)
            x = x_norm
        elif 'normsumpool' == self.global_pooling_mode:
            # normalized global sum pool
            
            # 0) flatten lattice
            x_norm = x.view(x.shape[0], x.shape[1], -1)
            
            # 1) normalize to [0, 1]
            x_min, _ = torch.min(x_norm, dim=2)
            x_norm = x_norm - x_min.view(*x_min.shape, 1)
            x_max, _ = torch.max(x_norm, dim=2)
            x_norm = x_norm / (x_max.view(*x_max.shape, 1) + 1e-8)
            
            # 2) take global sim
            x_sum = torch.sum(x_norm, dim=2)
            x = x_sum
            x = x_min
        
        # dense network
        x = self.dense(x)
        
        # prediction
        x = torch.sigmoid(x)
        
        return x
    
    def loss(self, x, y_true):
        y_pred = self(x).flatten()
        y_true = torch.clamp(y_true, 0, 1)
        loss = torch.nn.functional.binary_cross_entropy(y_pred, y_true)
        return loss
    
    def correct_predictions(self, x, y_true):
        y_pred = self(x).flatten().round()
        y_true = torch.clamp(y_true, 0, 1).round()
        correct = (y_pred == y_true).float().sum()
        return correct

    """
        pytorch_lightning methods
    """

    def prepare_data(self):
        pass

    def train_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.train_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=True, num_workers=self.hparams.num_workers)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.val_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.test_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def configure_optimizers(self):
        self._optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr,
                                            weight_decay=self.hparams.weight_decay, amsgrad=True)
        
        self._scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            self._optimizer,
            T_0=self.hparams.period_epochs,
            T_mult=1,
            eta_min=self.hparams.lr_min,
            last_epoch=-1
        )
        
        return {'optimizer': self._optimizer, 'lr_scheduler': self._scheduler}

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)

        logs = {'loss': loss.cpu().item()}
        return {'loss': loss, 'log': logs}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu().item()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu().item()
        val_acc = val_correct / len(self.val_data)
        
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def test_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu()
        val_acc = val_correct / len(self.test_data)
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def get_progress_bar_dict(self):
        # call .item() only once but store elements without graphs
        running_train_loss = self.trainer.running_loss.mean()
        avg_training_loss = running_train_loss.cpu().item() if running_train_loss is not None else float('NaN')
        lr = self._scheduler.get_last_lr()[0] if self._scheduler is not None else self.hparams.lr
        
        tqdm_dict = {
            'loss': '{:.2E}'.format(avg_training_loss),
            'val_loss': '{:.2E}'.format(self.vloss),
            'val_acc': '{:.2f}'.format(self.vacc),
            'lr': '{:.2E}'.format(lr),
        }

        if self.trainer.truncated_bptt_steps is not None:
            tqdm_dict['split_idx'] = self.trainer.split_idx

        if self.trainer.logger is not None and self.trainer.logger.version is not None:
            tqdm_dict['v_num'] = self.trainer.logger.version

        return tqdm_dict
    
    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
        
        
"""
    Non-equivariant model with flattening
"""


class WormClassifierNEQF(pl.LightningModule):
    """
        A pytorch-lightning model for worm classification. This model uses convolutions with strided pooling
        layers and flattening.
    """
    def __init__(self, hparams, train_data, val_data, test_data):
        super(WormClassifierNEQF, self).__init__()
        self.hparams = hparams
        
        # set dataset paths 
        self.train_data = train_data
        self.val_data = val_data
        self.test_data = test_data
        
        # copy hyper parameters
        self.biases = hparams.biases.copy()
        self.kernels = hparams.kernels.copy()
        self.channels = hparams.channels.copy()
        self.strided_pools = hparams.strided_pools.copy()
        self.dense_sizes = hparams.dense_sizes.copy()
        self.dense_biases = hparams.dense_biases.copy()
        self.nx = hparams.nx
        self.nt = hparams.nt
        
        """
            Convolutional network
        """
        # convolutional network is implemented as a pytorch Sequential module
        module_list = []
        
        # add number of inputs (4, due to kt, kx, lt, lx) to the channel list
        count_strided_pools = 0
        self.channels.insert(0, 4)
        for i, ks in enumerate(self.kernels):
            # set in and out channels of CConv2d
            in_ch = self.channels[i]
            out_ch = self.channels[i+1]
            
            # set bias of CConv2D
            bias = self.biases[i]
            
            # create and append to list of modules
            module_list.append(CConv2d(in_ch, out_ch, ks, bias))
            
            # append activation function
            module_list.append(nonlinearity())
            
            # strided pooling layer
            if self.strided_pools[i] is not None:
                count_strided_pools += 1
                pool_mode = self.strided_pools[i]
                
                if pool_mode == 'maxpool':
                    pool = torch.nn.MaxPool2d(2)
                elif pool_mode == 'avgpool':
                    pool = torch.nn.AvgPool2d(2)
                else:
                    print("Unknown strided pooling mode '{}'.".format(pool_mode))
                    
                module_list.append(pool)
        
        # python list is cast to Sequential module
        self.conv = torch.nn.Sequential(*module_list)
        
        """
            Flattening
        """
        # implemented in forward()
        
        
        """
            Dense network
        """
        # dense network is implemented as a pytorch Sequential module 
        module_list = []
        
        # add number of inputs from convolutional network and account for flattening
        num_lattice_sites = self.nx * self.nt
        final_lattice_sites = num_lattice_sites // (4 ** count_strided_pools)
        self.dense_sizes.insert(0, self.channels[-1] * final_lattice_sites)
        self.dense_biases.insert(0, False)
        
        # add number of outputs (1, for prediction)
        self.dense_sizes.append(1)
        self.dense_biases.append(False)
        
        for i in range(len(self.dense_sizes) - 1):
            # set in and out sizes of linear layer
            in_f = self.dense_sizes[i]
            out_f = self.dense_sizes[i+1]
            
            # create and append to list of modules
            module_list.append(torch.nn.Linear(in_f, out_f, bias=self.dense_biases[i]))
            
            # if there is more than one linear layer, add non-linear activations
            if i < len(self.dense_sizes) - 2:
                module_list.append(nonlinearity())
        
        # python list is cast to Sequential module
        self.dense = torch.nn.Sequential(*module_list)
        
        # metrics (loss and accuracy)
        self.vloss = 0.0
        self.vacc = 0.0
        

    def forward(self, x):
        # convolutional network
        x = self.conv(x)
        
        # flatten
        x_shape = x.shape
        x = x.view(x_shape[0], -1)
        
        # dense network
        x = self.dense(x)
        
        # prediction
        x = torch.sigmoid(x)
        
        return x
    
    def loss(self, x, y_true):
        y_pred = self(x).flatten()
        y_true = torch.clamp(y_true, 0, 1)
        loss = torch.nn.functional.binary_cross_entropy(y_pred, y_true)
        return loss
    
    def correct_predictions(self, x, y_true):
        y_pred = self(x).flatten().round()
        y_true = torch.clamp(y_true, 0, 1).round()
        correct = (y_pred == y_true).float().sum()
        return correct

    """
        pytorch_lightning methods
    """
    
    def prepare_data(self):
        pass

    def train_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.train_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=True, num_workers=self.hparams.num_workers)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.val_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.test_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def configure_optimizers(self):
        self._optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr,
                                            weight_decay=self.hparams.weight_decay, amsgrad=True)
        
        self._scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            self._optimizer,
            T_0=self.hparams.period_epochs,
            T_mult=1,
            eta_min=self.hparams.lr_min,
            last_epoch=-1
        )
        
        return {'optimizer': self._optimizer, 'lr_scheduler': self._scheduler}

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)

        logs = {'loss': loss.cpu().item()}
        return {'loss': loss, 'log': logs}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu().item()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu().item()
        val_acc = val_correct / len(self.val_data)
        
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def test_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        correct = self.correct_predictions(x, y)
        
        return {'val_loss': loss, 'val_correct': correct}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu()
        val_correct = torch.stack([x['val_correct'] for x in outputs]).sum().cpu()
        val_acc = val_correct / len(self.test_data)
        logs = {'val_loss': avg_loss, 'val_acc': val_acc}
        self.vloss = avg_loss
        self.vacc = val_acc
        
        return {'avg_val_loss': avg_loss, 'val_acc': val_acc, 'log': logs}

    def get_progress_bar_dict(self):
        # call .item() only once but store elements without graphs
        running_train_loss = self.trainer.running_loss.mean()
        avg_training_loss = running_train_loss.cpu().item() if running_train_loss is not None else float('NaN')
        lr = self._scheduler.get_last_lr()[0] if self._scheduler is not None else self.hparams.lr
        
        tqdm_dict = {
            'loss': '{:.2E}'.format(avg_training_loss),
            'val_loss': '{:.2E}'.format(self.vloss),
            'val_acc': '{:.2f}'.format(self.vacc),
            'lr': '{:.2E}'.format(lr),
        }

        if self.trainer.truncated_bptt_steps is not None:
            tqdm_dict['split_idx'] = self.trainer.split_idx

        if self.trainer.logger is not None and self.trainer.logger.version is not None:
            tqdm_dict['v_num'] = self.trainer.logger.version

        return tqdm_dict
    
    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
