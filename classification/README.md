# Classification task

## Setup

Firstly, download and extract the dataset from 
[this repository](https://zenodo.org/record/4644550) at Zenodo. Please change the
`path_data` variable in `datapaths.py` accordingly.


Secondly, install and activate the conda environment provided in the yaml file.

```
conda env create -f env.yml
conda activate scalar_ml_classification_v1
```

## Running optuna searches

We use `optuna` to search for well-performing architectures in a given search space
as defined by `optuna_cmd.py`. In order to re-run the searches we performed we have
provided a batch script called `run_all_optuna.sh`. For each type we perform two
independent searches.

Note: depending on your hardware the architecture search can take a few days.

## Re-training

After the architecture search is done, we select the best architecture of all runs
of each of the three types (EQ, ST and FLAT as defined in the paper) and re-train them.
This creates an ensemble of models for each type.
The code for this is found in `retrain_cmd_hparams.py` and the batch script `retrain_all.sh`.

## Testing

Finally, we test our ensembles of models using `test_cmd.py` and the batch script
`test_all.sh`.

## Results

The final results are gathered and organized for plotting using 

## Code overview

- `cconv.py` definitions for convolutional and pooling layers with circular padding
- `datapaths.py` defines the path to the `violations` dataset
- `models.py` PyTorch Lightning models for each architecture type
- `optuna_cmd.py` command line script for running optuna architecture searches
- `retrain_cmd_hparams.py` command line script for retraining given architectures
- `test_cmd.py` command line script for running tests on trained models