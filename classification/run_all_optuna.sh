# This script runs two optuna searches for each architecture type (EQ, ST, FLAT)
# See 'train_cmd_optuna.py' for an explanation of the arguments.

# equivariant architectures
python optuna_cmd.py "eq_search_1" "equivariant" 1000 500 100 5 200
python optuna_cmd.py "eq_search_2" "equivariant" 1000 500 100 5 200

# strided architectures
python optuna_cmd.py "st_search_1" "nonequivariant_gp" 1000 500 100 5 200
python optuna_cmd.py "st_search_2" "nonequivariant_gp" 1000 500 100 5 200

# flatenning architectures
python optuna_cmd.py "flat_search_1" "nonequivariant_fl" 1000 500 100 5 200
python optuna_cmd.py "flat_search_2" "nonequivariant_fl" 1000 500 100 5 200
