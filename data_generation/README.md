# data_generation

A program that simulates a complex scalar field on the lattice in 2, 3 or 4 dimensions via the worm algorithm. The theoretical background is based on this article: [https://arxiv.org/pdf/1206.2954.pdf](https://arxiv.org/pdf/1206.2954.pdf).
The main code is written in C++ and the script works in Python 3, so make sure to have installed the g++ compiler and a version of Python 3.

## Launching the simulation

To launch a script module from the command line, go to the root directory of this repository. You can explore the possible arguments that can be passed to the program writing:

    python3 script.py -h
    
For example, to make a comparison with [Gattringer's article](https://arxiv.org/pdf/1206.2954.pdf) you can type:

    python3 script.py 9 1 1.12 100 4 4 4 -m 0.01 1.18 --config False
    
## Regression task dataset

Our dataset for the first task can be generated with the following scripts.


50x2 lattices:

    python3 script.py 4.01 1 0.91 50 2 -m 0.005 1.045
    python3 script.py 4.01 1 1.05 50 2 --ndata 26000
    
60x4 lattices:

    python3 script.py 4.01 1 0.91 60 4 -m 0.005 1.045
    python3 script.py 4.01 1 1.05 60 4 --ndata 26000
    
100x5 lattices:

    python3 script.py 4.01 1 0.91 100 5 -m 0.005 1.045
    python3 script.py 4.01 1 1.05 100 5 --ndata 26000
    
125x8 lattices:

    python3 script.py 4.01 1 0.91 125 8 -m 0.005 1.045
    python3 script.py 4.01 1 1.05 125 8 --ndata 26000

200x10 lattices:

    python3 script.py 4.01 1 0.91 200 10 -m 0.005 1.045
    python3 script.py 4.01 1 1.05 200 10 --ndata 26000

For some specific values of the chemical potential and of the lattice sizes the dataset can suffer from high autocorrelation. For such values we increase the sweeps between each measurements to 50 and recreate the dataset, e.g. with the script

    python3 script.py 4.01 1 0.91 200 10 -s 50

## Dataset with flux violations

In this part we document how to create datasets that are used both in the second and third tasks of our paper. The following script generates configurations without flux violations for all the combinations of physical parameters.

    for dim in 8 16 32 64; do for eta in 4.01 4.04 4.25; do python3 script.py $eta 1 1 $dim $dim -m 0.25 1.5 --ndata 100 --obs false --config true --sweeps 100 --thermal_skip 2000; done; done

Configurations with flux violations are given by

    for n in {1..10}; do for dim in 8 16 32 64; do for eta in 4.01 4.04 4.25; do python3 script.py $eta 1 1 $dim $dim -m 0.25 1.5 --ndata 100 --obs false --config false --open_config true --open_worms $n --sweeps 100 --thermal_skip 2000; done; done; done

The training sets consist of 10000 samples each and are created with the following.

    python3 script.py 4.01 1 1.5 8 8 --ndata 10000 --obs false --config true --sweeps 100 --thermal_skip 2000
    
    python3 script.py 4.25 1 1 8 8 --ndata 10000 --obs false --config true --sweeps 100 --thermal_skip 2000
    
    for n in 1 5; do python3 script.py 4.01 1 1.5 8 8 --ndata 10000 --obs false --config false --open_config true --open_worms $n --sweeps 100 --thermal_skip 2000; done
    
    for n in 1 5; do python3 script.py 4.25 1 1 8 8 --ndata 10000 --obs false --config false --open_config true --open_worms $n --sweeps 100 --thermal_skip 2000; done
    
Note that these last scripts overwrite datasets with the same physical parameters created with the previous scripts, so make sure not to invert the order of generation.