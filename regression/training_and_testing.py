import torch, random
import pytorch_lightning as pl, numpy as np


class CConv2DDil(torch.nn.Module):
    """
        A wrapper for Conv2D which correctly implements circular padding and dilated convolutions.
    """
    def __init__(self, in_channels, out_channels, kernel_size, dilation=1, bias=True):
        super().__init__()
        
        # Conv2D without padding
        self.conv = torch.nn.Conv2d(in_channels, out_channels, kernel_size, dilation=dilation, bias=bias)
        
        # padding size
        self.padding = []
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            k = dilation*(k - 1) + 1
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_conv = self.conv(x_pad)
        return x_conv
    

class AugmentedDataset(torch.utils.data.Dataset):
    """
        A class that enables to augment the training data by shifting it.
    """
    def __init__(self, dataset, augment=False, flattening=False):
        self.dataset = dataset
        self.augment = augment
        self.flattening = flattening
        
    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        x, y = self.dataset[idx]
        
        if self.augment:
            if not self.flattening:
                x = torch.roll(x, shifts=(random.randint(0,3), random.randint(0,3)), dims=(1,2))
            else:
                x = torch.roll(x, shifts=(random.randint(0,59), random.randint(0,3)), dims=(1,2))
            
        return x, y
    
    
def update_kernel_size(kernel_size):
    new_kernel_size = kernel_size
    if type(kernel_size) == int:
        new_kernel_size = [kernel_size, kernel_size]
    elif type(kernel_size) == list:
        if len(kernel_size) == 1:
            new_kernel_size = [kernel_size[0], kernel_size[0]]
    return new_kernel_size


class CMaxPool2d(torch.nn.Module):
    """
        A wrapper for MaxPool2d which correctly implements circular padding.
    """
    def __init__(self, kernel_size, stride):
        super(CMaxPool2d, self).__init__()
        
        # MaxPool2d without padding
        self.pool = torch.nn.MaxPool2d(kernel_size, stride)
        
        # padding size
        self.padding = []
        
        # Check and fix size of kernel_size
        kernel_size = update_kernel_size(kernel_size)
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_pool = self.pool(x_pad)
        return x_pool


class CAvgPool2d(torch.nn.Module):
    """
        A wrapper for AvgPool2d which correctly implements circular padding.
    """
    def __init__(self, kernel_size, stride):
        super(CAvgPool2d, self).__init__()
        
        # AvgPool2d without padding
        self.pool = torch.nn.AvgPool2d(kernel_size, stride)
        
        # padding size
        self.padding = []
        
        # Check and fix size of kernel_size
        kernel_size = update_kernel_size(kernel_size)
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_pool = self.pool(x_pad)
        return x_pool
    
    
class ObsPredictor(pl.LightningModule):
    """
        A pytorch-lightning model for regression - predicting 2 numerical values.
        This model is equipped for translationally equivariant convolutional architectures if one
        chooses to use a global average pooling layer at the end of the convolutional layers,
        but there is also the possibility to break it by inserting spatial pooling layers.
        It has an optional dense network at the end of the graph.
        Instead of the usage of a global pooling layer, the output of the convolutional layers
        can also be flattened before feeding it to the dense network.
        This also breaks translational invariance.
    """
    def __init__(self, hparams, train_data, val_data, test_data):
        super().__init__()
        self.hparams = hparams
        
        # set dataset paths 
        self.train_data = train_data
        self.val_data = val_data
        self.test_data = test_data
        
        # copy hyper parameters
        self.kernels = hparams.kernels.copy()
        self.channels = hparams.channels.copy()
        if hparams.dense_sizes is None:
            self.dense_sizes = hparams.dense_sizes
        else:
            self.dense_sizes = hparams.dense_sizes.copy()
            
        self.spatial_pool = hparams.spatial_pool.copy()
        
        self.loss_function = torch.nn.MSELoss()
        
        """
            Convolutional network
        """
        
        # convolutional network is implemented as a pytorch Sequential module
        module_list = []
        
        # add number of inputs (4, due to kt, kx, lt, lx) to the channel list
        self.channels.insert(0, 4)
        for i, ks in enumerate(self.kernels):
            # set in and out channels of CConv2DDil
            in_ch = self.channels[i]
            out_ch = self.channels[i+1]
            
            # create and append to list of modules (bias is always used)
            module_list.append(CConv2DDil(in_ch, out_ch, ks, bias=True))
            
            # append LeakyReLU activation function
            module_list.append(torch.nn.LeakyReLU())
            
            # add potential spatial pooling layers
            if self.spatial_pool[i] == 'avg':
                module_list.append(torch.nn.AvgPool2d(2))
            elif self.spatial_pool[i] == 'max':
                module_list.append(torch.nn.MaxPool2d(2))
            elif self.spatial_pool[i] == 'avgs1':
                module_list.append(CAvgPool2d(kernel_size=2, stride=1))
            elif self.spatial_pool[i] == 'maxs1':
                module_list.append(CMaxPool2d(kernel_size=2, stride=1))
        
        # python list is cast to Sequential module
        self.conv = torch.nn.Sequential(*module_list)
        
        """
            Dense network
        """
        
        # the dense network is implemented as a pytorch Sequential module
        # if the output should be taken directly after the global average pooling, self.dense_sizes is None
        
        if self.dense_sizes is not None:
            module_list = []

            # add number of inputs from convolutional network
            if self.hparams.flattening:
                # the flattening_correction is used to determine the number of input nodes of the first dense layer
                # for Global Average Pooling, the latter is equal to the number of channels of the last convolutional layer
                # for flattening, one has to multiply it by the output size of the last convolution
                num_spatial_pool_layers = self.spatial_pool.count('avg') + self.spatial_pool.count('max')
                flattening_correction = np.array([self.hparams.NT, self.hparams.NX], dtype=int)
                for _ in range(num_spatial_pool_layers):
                    # this holds for a 2x2 kernel with a stride of 2
                    # also, the convolutions do not change the size of their respective input
                    flattening_correction = flattening_correction//2
                self.dense_sizes.insert(0, self.channels[-1]*np.prod(flattening_correction))
            else:
                self.dense_sizes.insert(0, self.channels[-1])

            # add number of outputs (2, for prediction)
            self.dense_sizes.append(2)

            for i in range(len(self.dense_sizes) - 1):
                # set in and out sizes of linear layer
                in_f = self.dense_sizes[i]
                out_f = self.dense_sizes[i+1]

                # create and append to list of modules (bias is always used)
                module_list.append(torch.nn.Linear(in_f, out_f, bias=True))

                # if there is more than one linear layer, add non-linear activations
                if i < len(self.dense_sizes) - 2:
                    module_list.append(torch.nn.LeakyReLU())

            # python list is cast to Sequential module
            self.dense = torch.nn.Sequential(*module_list)
        
        
        # metrics (loss and MSE)
        self.vloss = 0.0
        self.vMSE = [0.0, 0.0]
        

    def forward(self, x):
        # convolutional network
        x = self.conv(x)
        if self.hparams.flattening:
            x = x.view(x.shape[0], -1)
            # dense network
            if self.dense_sizes is None:
                raise NotImplementedError('The combination of FLATTENING and dense_sizes = None is not allowed.')
            else:
                return self.dense(x)
        else:
            # GAP
            x = x.view(x.shape[0], x.shape[1], -1)
            x = torch.mean(x, dim=2)
            # dense network
            if self.dense_sizes is None:
                return x
            else:
                return self.dense(x)
    
    def loss(self, x, y_true):
        y_pred = self(x)
        loss = self.loss_function(y_pred, y_true)
        return loss
    
    
    def MSE_per_pred(self, x, y_true):
        y_pred = self(x)
        return torch.mean(torch.nn.MSELoss(reduction='none')(y_pred, y_true), dim = 0)
    
    
    """
        pytorch_lightning methods
    """

    def prepare_data(self):
        pass

    def train_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.train_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=True, num_workers=self.hparams.num_workers)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.val_data,
                                           batch_size=self.hparams.batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.test_data,
                                           batch_size=self.hparams.test_batch_size,
                                           shuffle=False, num_workers=self.hparams.num_workers)

    def configure_optimizers(self):
        self._optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr,
                                            weight_decay=self.hparams.weight_decay, amsgrad=True)
        return {'optimizer': self._optimizer}

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)

        return {'loss': loss}
    
    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean().cpu().item()
        self.logger.experiment.add_scalar("loss", avg_loss, self.current_epoch)
        
        return {'loss': avg_loss}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        MSE_per_pred = self.MSE_per_pred(x, y)
        
        return {'val_loss': loss, 'MSE': MSE_per_pred}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean().cpu().item()
        avg_MSE = torch.mean(torch.stack([x['MSE'] for x in outputs]).cpu(), axis=0)
        avg_MSE_n, avg_MSE_phi2 = avg_MSE
        
        self.vloss = avg_loss
        self.vMSE = avg_MSE
        
        self.logger.experiment.add_scalar("val_loss", avg_loss, self.current_epoch)
        self.logger.experiment.add_scalar("avg_MSE_n", avg_MSE_n, self.current_epoch)
        self.logger.experiment.add_scalar("avg_MSE_phi2", avg_MSE_phi2, self.current_epoch)

        return {'val_loss': avg_loss, 'avg_MSE_n': avg_MSE_n, 'avg_MSE_phi2': avg_MSE_phi2}

    def test_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        MSE_per_pred = self.MSE_per_pred(x, y)
        
        return {'test_loss': loss, 'MSE': MSE_per_pred}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean().cpu().item()
        avg_MSE = torch.mean(torch.stack([x['MSE'] for x in outputs]).cpu(), axis=0)
        avg_MSE_n, avg_MSE_phi2 = avg_MSE

        logs = {'test_loss': avg_loss, 'test_MSE_n': avg_MSE_n.item(), 'test_MSE_phi2': avg_MSE_phi2.item()}
        self.vloss = avg_loss
        self.vMSE = avg_MSE
        
        return {'log': logs}

    def get_progress_bar_dict(self):
        # call .item() only once but store elements without graphs
        running_train_loss = self.trainer.running_loss.mean()
        avg_training_loss = running_train_loss.cpu().item() if running_train_loss is not None else float('NaN')
        lr = self.hparams.lr

        tqdm_dict = {
            'loss': '{:.2E}'.format(avg_training_loss),
            'val_loss': '{:.2E}'.format(self.vloss),
            'val_MSE': '{:.2f}'.format(self.vMSE),
            'lr': '{:.2E}'.format(lr),
        }

        if self.trainer.truncated_bptt_steps is not None:
            tqdm_dict['split_idx'] = self.trainer.split_idx

        if self.trainer.logger is not None and self.trainer.logger.version is not None:
            tqdm_dict['v_num'] = self.trainer.logger.version

        return tqdm_dict
    
    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)