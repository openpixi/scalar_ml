# Regression task

## Setup

Download and extract the regression dataset from [this repository](https://zenodo.org/record/4644550) at Zenodo. Do change the `path_data` variable in `Training and Testing.ipynb` and `Optuna.ipynb` accordingly.

The conda environment that we used for this task is provided in the `env.yml` file. It is the same one as for the counting task. Thus, if it already exists, it only needs to be activated via shell with:

```
conda activate scalar_ml_counting_v1
```

If not, it needs to be created first:

```
conda env create -f env.yml
conda activate scalar_ml_counting_v1
```

## Training and testing models

In the jupyter notebook `Training and Testing.ipynb` various models can be trained and tested. Additional classes and functions are stored in the corresponding python file `training_and_testing.py`.

If the notebook is executed with an architecture of type EQ, it will create two `.pickle` files: one containing the test results on the $`60 \times 4`$ lattice and one containing the test results on other lattice sizes. Note that if you choose an architecture of type ST that contains more than one $`2 \times 2`$ spatial pooling layer, the testing cannot be performed on the $`50 \times 2`$ lattice, so make sure to change the corresponding `dims` variable accordingly. Also note that FLAT architectures cannot be tested on other lattice sizes, thus for them, the output of this notebook will only be one `.pickle` file. The definition of the different architecture types can be found in the paper.

The test results are displayed in the jupyter notebook `Test Plots.ipynb`. In order to do that, the `.pickle` files that shall be plotted have to be added to the lists `filenames` and `ls_filenames`.

## Looking for good architectures with Optuna

We use `optuna` to look for well-performing architectures. The jupyter notebook `Optuna.ipynb` contains such a search for EQ architectures. It produces one `.pickle` file for each number of training samples that is chosen. Additional classes and functions are stored in `Optuna.py`.

The results of the optuna search are analyzed in `Optuna results.ipynb`. There, `name_string_helper` and `train_sample_numbers` determine which `.pickle` files shall be loaded and which results shall be displayed.

## Code overview

* `Optuna results.ipynb` displays the results of optuna searches
* `Optuna.ipynb` launches an optuna search
* `Optuna.py` contains classes and functions for `Optuna.ipynb`
* `Test Plots.ipynb` displays the test results of models trained in `Training and Testing.ipynb`
* `Training and Testing.ipynb` trains and tests models of an architecture that has to be specified
* `training_and_testing.py` contains classes and functions for `Training and Testing.ipynb`
