# Generalization capabilities of translationally equivariant neural networks
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4644550.svg)](https://doi.org/10.5281/zenodo.4644550)

This repository contains example code for our paper "Generalization capabilities of translationally equivariant neural networks" ([arXiv:2103.14686](https://arxiv.org/abs/2103.14686)).
Our datasets can be found in [this repository](https://zenodo.org/record/4644550) at Zenodo.

The folder `data_generation` contains the code that was used to generate the datasets that have been used in the machine learning tasks in this paper. The folders `regression`, `classification` and `counting` contain code that shows how we have tackled these tasks.
